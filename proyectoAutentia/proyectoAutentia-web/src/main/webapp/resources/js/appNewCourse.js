(function() {
	angular.module('proyectoAutentia', ['angularFileUpload'])
	.controller('NewCourseController', ['$scope', 'FileUploader', '$http', function($scope, FileUploader, $http) {
		
		var ncc = this;
		
		ncc.uploader = new FileUploader({
            'url': 'http://localhost:8080/proyectoAutentia/fileUpload.html'
//            ,'data' : {'course':ncc.formNewCourse}
        });

        
		ncc.uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
	            console.info('onWhenAddingFileFailed', item, filter, options);
	        };
	        ncc.uploader.onAfterAddingFile = function(fileItem) {
	            console.info('onAfterAddingFile', fileItem);
	        };
	        ncc.uploader.onAfterAddingAll = function(addedFileItems) {
	            console.info('onAfterAddingAll', addedFileItems);
	        };
	        ncc.uploader.onBeforeUploadItem = function(item) {
	            console.info('onBeforeUploadItem', item);
	        };
	        ncc.uploader.onProgressItem = function(fileItem, progress) {
	            console.info('onProgressItem', fileItem, progress);
	        };
	        ncc.uploader.onProgressAll = function(progress) {
	            console.info('onProgressAll', progress);
	        };
	        ncc.uploader.onSuccessItem = function(fileItem, response, status, headers) {
	            console.info('onSuccessItem', fileItem, response, status, headers);
	        };
	        ncc.uploader.onErrorItem = function(fileItem, response, status, headers) {
	            console.info('onErrorItem', fileItem, response, status, headers);
	        };
	        ncc.uploader.onCancelItem = function(fileItem, response, status, headers) {
	            console.info('onCancelItem', fileItem, response, status, headers);
	        };
	        ncc.uploader.onCompleteItem = function(fileItem, response, status, headers) {
	            console.info('onCompleteItem', fileItem, response, status, headers);
	        };
	        ncc.uploader.onCompleteAll = function() {
	            console.info('onCompleteAll');
	            
	            for (i = 0; i < ncc.uploader.queue.length; i++) { 
	            	ncc.uploader.removeFromQueue(0);
	            }
	            
	        };

			
			ncc.formNewCourse = {};
			
			ncc.sendToServer = function() {
				$http({
					'url' : 'http://localhost:8080/proyectoAutentia/newcourse.json',
					'method' : 'POST',
					'headers': {'Content-Type' : 'application/json'},
					'data' : ncc.formNewCourse
				})
				.success(function(data, status, headers, config) {
					ncc.uploader.uploadAll();
					ncc.formNewCourse = {};
				}).error(function(data, status, headers, config) {
				});
			};

    }])
	
})();