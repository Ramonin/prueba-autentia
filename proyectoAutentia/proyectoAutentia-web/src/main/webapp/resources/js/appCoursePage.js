(function() {
	var app = angular.module('proyectoAutentia', []);
	
	app.controller('AppController', function($scope, FileUploader) {
        $scope.uploader = new FileUploader();
    });
	

	app.controller('CourseController', [ '$http', function($http) {
		var cc = this;
		cc.courses = [];
		
		cc.numpage = 1;
		cc.pages = [];

		$http.get('http://localhost:8080/proyectoAutentia/listcourses.json?numPage='+cc.numpage)
		.success(function(data) {
			cc.courses = data.courses;
			cc.setPages(data.maxPages);
		}).error(function(data) {
		});

		
		cc.predicate = '-hours';		
		
		cc.reverse = false;

		
		cc.setData = function(numpage) {
			cc.numpage = numpage;
			$http.get('http://localhost:8080/proyectoAutentia/listcourses.json?numPage='+cc.numpage+'&atrOrderBy='+cc.predicate+'&ascDesc='+cc.reverse)
			.success(function(data) {
				cc.courses = data.courses;
				cc.setPages(data.maxPages);
			}).error(function(data) {
			});
		};
		
		cc.setPages = function(maxpage) {
			cc.pages = [];
			for (var i = 1; i < maxpage+1; i++) {
				cc.pages.push(i);
			}
		};
		
	} ]);

	
})();