<%@page
	contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="com.ramoninvarato.core.model.Course.*"%>
<%@taglib
	prefix="c"
	uri="http://java.sun.com/jsp/jstl/core"%>

<html ng-app="proyectoAutentia">
<head>

<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

<!-- Latest compiled and minified CSS -->
<link
	rel="stylesheet"
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<!-- Optional theme -->
<link
	rel="stylesheet"
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.19/angular.min.js"></script>

<script src="<c:url value="/resources/js/appCoursePage.js" />"></script>

<link
	rel="stylesheet"
	type="text/css"
	href="<c:url value="/resources/css/main.css" />">

<title>Cat�logo de cursos</title>
</head>
<body>
	<h1>Cat�logo de cursos</h1>
	<div ng-controller="CourseController as cc">
		<!-- 	<p>{{cc.product.name}}</p> -->
		<!-- 	<p>{{cc.predicate}}</p> -->
		<!-- 			<div -->
		<!-- 				ng-repeat="course in cc.courses | orderBy:predicate:reverse"> -->
		<!-- 				<p>{{course.title}}</p> -->
		<!-- 				<p>{{course.level}}</p> -->
		<!-- 				<p>{{course.hours}}</p> -->
		<!-- 			</div> -->


		<!-- 		<pre>Sorting predicate = {{cc.predicate}}; reverse = {{cc.reverse}}; {{cc.isOrderAsc(predicate)==-1}} dd{{cc.isOrderAsc(predicate)}} Page: {{cc.numpage}} -- {{cc.numpage==1}} ppppp {{cc.maxpage}}</pre> -->
		<table class="table table-striped">
			<tr>
				<th><a
					href=""
					ng-click="cc.predicate = 'title'; cc.reverse=!cc.reverse; cc.setData(cc.numpage)">T�tulo</a> <span
					ng-show="cc.predicate != 'title'"
					class="glyphicon glyphicon-sort"></span> <span
					ng-show="cc.predicate == 'title' && cc.reverse"
					class="glyphicon glyphicon-sort-by-alphabet"></span> <span
					ng-show="cc.predicate == 'title' && !cc.reverse"
					class="glyphicon glyphicon-sort-by-alphabet-alt"></span></th>
				<th><a
					href=""
					ng-click="cc.predicate = 'level'; cc.reverse=!cc.reverse; cc.setData(cc.numpage)">Nivel</a> <span
					ng-show="cc.predicate != 'level'"
					class="glyphicon glyphicon-sort"></span> <span
					ng-show="cc.predicate == 'level' && cc.reverse"
					class="glyphicon glyphicon-sort-by-alphabet"></span> <span
					ng-show="cc.predicate == 'level' && !cc.reverse"
					class="glyphicon glyphicon-sort-by-alphabet-alt"></span></th>
				<th><a
					href=""
					ng-click="cc.predicate = 'hours'; cc.reverse=!cc.reverse; cc.setData(cc.numpage)">Horas</a> <span
					ng-show="cc.predicate != 'hours'"
					class="glyphicon glyphicon-sort"></span> <span
					ng-show="cc.predicate == 'hours' && cc.reverse"
					class="glyphicon glyphicon-sort-by-alphabet"></span> <span
					ng-show="cc.predicate == 'hours' && !cc.reverse"
					class="glyphicon glyphicon-sort-by-alphabet-alt"></span></th>
				<th>Fichero</th>
			</tr>

			<tr ng-repeat="course in cc.courses | orderBy:cc.predicate:cc.reverse">
				<td>{{course.title}}</td>
				<td>{{course.level}}</td>
				<td>{{course.hours}}</td>
<%-- 				<td><a href="<c:url value="/files/" />{{course.attachedfile}}">Descargar</a></td> --%>
				<td ng-show="course.attachedfile"><a href="<c:url value="/files/" />{{course.attachedfile}}">Descargar</a></td>
				<td ng-hide="course.attachedfile"></td>
			</tr>
		</table>

		<ul class="pagination">
			<li
				ng-show="cc.numpage == 1"
				class="disabled"><a href="#">&laquo;</a></li>
			<li ng-hide="cc.numpage == 1"><a
				href="#"
				ng-click="cc.setData(cc.numpage-1)">&laquo;</a></li>

			<li
				ng-repeat-start="page in cc.pages"
				ng-show="cc.numpage == {{page}}"
				class="active"><a
				href="#"
				ng-click="cc.setData(page)">{{page}}</a></li>
			<li
				ng-repeat-end
				ng-hide="cc.numpage == {{page}}"
				ng-hide="cc.numpage == {{page}}"><a
				href="#"
				ng-click="cc.setData(page)">{{page}}</a></li>

			<li
				ng-show="cc.numpage == cc.pages.length"
				class="disabled"><a href="#">&raquo;</a></li>
			<li ng-hide="cc.numpage == cc.pages.length"><a
				href="#"
				ng-click="cc.setData(cc.numpage+1)">&raquo;</a></li>
		</ul>

	</div>

	<a
		class="btn btn-primary"
		href="<c:url value="/newCourse.html" />">Nuevo Curso</a>

</body>
</html>
