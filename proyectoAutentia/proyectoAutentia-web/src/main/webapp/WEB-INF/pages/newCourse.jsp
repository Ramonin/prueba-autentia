<%@page
	contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="com.ramoninvarato.core.model.Course.*"%>
<%@taglib
	prefix="c"
	uri="http://java.sun.com/jsp/jstl/core"%>

<html ng-app="proyectoAutentia">
<head>

<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

<!-- Latest compiled and minified CSS -->
<link
	rel="stylesheet"
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<!-- Optional theme -->
<link
	rel="stylesheet"
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.19/angular.min.js"></script>

<script src="<c:url value="/resources/js/angular-file-upload.min.js" />"></script>


<script src="<c:url value="/resources/js/appNewCourse.js" />"></script>

<link
	rel="stylesheet"
	type="text/css"
	href="<c:url value="/resources/css/main.css" />">

<title>Nuevo cursos</title>
</head>
<body>
	<h1>Nuevo cursos</h1>

	<form
		role="form"
		ng-controller="NewCourseController as ncc"
		ng-submit="ncc.sendToServer()"
		enctype="multipart/form-data"
		novalidate>

		<div
			class="checkbox"
			id="idEnable">
			<label> <input
				type="checkbox"
				ng-model="ncc.formNewCourse.enable" /> Activo
			</label>
		</div>

		<div class="form-group">
			<label for="idTeacher">Profesor</label> <select
				class="form-control"
				id="idTeacher"
				ng-model="ncc.formNewCourse.idTeacherKeyForeign"
				required>
				<c:forEach
					var="teacher"
					items="${teachers}">
					<option value="${teacher.idTeacher}">${teacher.name}</option>
				</c:forEach>
			</select>
		</div>
		<div class="form-group">
			<label for="idTitule">T�tulo</label> <input
				type="text"
				class="form-control"
				id="exampleInputEmail1"
				placeholder="Nombre del curso"
				id="idTitule"
				ng-model="ncc.formNewCourse.title"
				required>
		</div>
		<div class="form-group">
			<label for="idLevel">Nivel</label> <select
				class="form-control"
				id="idLevel"
				ng-model="ncc.formNewCourse.level"
				required>
				<option value="Basico">B�sico</option>
				<option value="Intermedio">Intermedio</option>
				<option value="Avanzado">Avanzado</option>
			</select>
		</div>
		<div class="form-group">
			<label for="idHours">Horas</label> <input
				type="number"
				min="0"
				class="form-control"
				id="idHours"
				placeholder="Horas"
				ng-model="ncc.formNewCourse.hours"
				required>
		</div>

		<div
			class="form-group">
			<label for="idFile">A�adir</label> <input
				id="idFile"
				type="file"
				nv-file-select
				uploader="ncc.uploader" />
			<ul>
				<li ng-repeat="item in ncc.uploader.queue">{{ item.file.name }}</li>
			</ul>
		</div>

		<button
			type="submit"
			class="btn btn-primary">Nuevo Curso</button>
	</form>



</body>
</html>
