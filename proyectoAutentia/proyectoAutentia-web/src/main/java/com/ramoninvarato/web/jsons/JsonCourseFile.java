package com.ramoninvarato.web.jsons;

import org.apache.commons.fileupload.FileUpload;

import com.ramoninvarato.core.model.Course;

public class JsonCourseFile extends Course  {

	private static final long serialVersionUID = -3380745916621769541L;
	
	private FileUpload file;

	public FileUpload getFile() {
		return file;
	}

	public void setFile(FileUpload file) {
		this.file = file;
	}

}
