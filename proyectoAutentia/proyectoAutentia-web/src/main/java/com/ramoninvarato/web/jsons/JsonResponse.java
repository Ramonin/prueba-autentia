package com.ramoninvarato.web.jsons;

import java.io.Serializable;

public class JsonResponse implements Serializable {

	private static final long serialVersionUID = -3003226810154368245L;
	
	private String response;

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

}