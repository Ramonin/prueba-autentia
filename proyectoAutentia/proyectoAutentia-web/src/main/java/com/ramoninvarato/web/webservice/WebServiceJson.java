package com.ramoninvarato.web.webservice;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileUploadException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.ramoninvarato.core.controller.CoursesController;
import com.ramoninvarato.core.controller.CoursesController.ORDER;
import com.ramoninvarato.core.controller.CoursesController.TABLEBYORDER;
import com.ramoninvarato.core.controller.TeachersController;
import com.ramoninvarato.core.model.Course;
import com.ramoninvarato.core.model.Teacher;
import com.ramoninvarato.web.file.FileValidator;
import com.ramoninvarato.web.file.UploadedFile;
import com.ramoninvarato.web.jsons.JsonCantCourses;
import com.ramoninvarato.web.jsons.JsonResponse;

/**
 * http://localhost:8080/proyectoAutentia/listcourses.json?numPage=1
 * 
 * http://localhost:8080/proyectoAutentia/coursesPage.html
 * 
 * http://localhost:8080/proyectoAutentia/pages/newCourse.html
 * 
 * @author Ram�n Invarato Men�ndez
 * 
 */
@Controller
public class WebServiceJson {

	@Autowired
	private TeachersController mTc;

	@Autowired
	private CoursesController mCc;

	@RequestMapping("/coursesPage.html")
	public ModelAndView coursesPage(HttpServletRequest request) {
		return new ModelAndView("coursesPage", CoursesController.class.getName(), null);
	}

	@RequestMapping("/newCourse.html")
	public ModelAndView newCourse(HttpServletRequest request) {
		List<Teacher> teachers = mTc.searchAllTeachers();

		ModelAndView modelAndView = new ModelAndView("newCourse",
				CoursesController.class.getName(), null);
		modelAndView.addObject("teachers", teachers);
		return modelAndView;
	}

	@RequestMapping(value = "/listcourses.json", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody JsonCantCourses getParamsListCourses(
			@RequestParam(value = "numPage", required = true) int numPage,
			@RequestParam(value = "atrOrderBy", required = false) String atrOrderBy,
			@RequestParam(value = "ascDesc", required = false) Boolean ascDesc) {

		TABLEBYORDER tablebyorder;
		if (atrOrderBy == null) {
			tablebyorder = null;
		} else {
			switch (atrOrderBy) {
			case "title":
				tablebyorder = TABLEBYORDER.title;
				break;
			case "hours":
				tablebyorder = TABLEBYORDER.hours;
				break;
			case "level":
				tablebyorder = TABLEBYORDER.level;
				break;
			default:
				tablebyorder = null;
				break;
			}
		}

		ORDER order;
		if (ascDesc != null && ascDesc) {
			order = ORDER.ASC;
		} else {
			order = ORDER.ASC;
		}

		List<Course> courses;
		if (tablebyorder == null) {
			courses = mCc.searchCoursesByPage(numPage);
		} else {
			courses = mCc.searchCoursesByPage(numPage, tablebyorder, order);
		}

		for (Course course : courses) {
			System.out.println("course: " + course);
		}

		JsonCantCourses jcc = new JsonCantCourses();
		jcc.setCourses(courses);
		jcc.setMaxPages(mCc.maxPages());

		return jcc;
	}


	@RequestMapping(value = "/newcourse.json", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody JsonResponse setNewCourse(@RequestBody Course course) {
		System.out.println("newcourse:"+course);
		
		ponerCurso(course, null);
//		mCc.saveCourse(course);

		JsonResponse jr = new JsonResponse();
		jr.setResponse("ok");
		return jr;
	}


	@Autowired
	FileValidator fileValidator;

	@RequestMapping(value = "/fileUpload.html", method = RequestMethod.POST)
	public JsonResponse fileUploaded(HttpServletRequest request,
			@ModelAttribute("uploadedFile") UploadedFile uploadedFile, BindingResult result) throws FileUploadException, IOException {
		
		MultipartFile file = uploadedFile.getFile();
		fileValidator.validate(uploadedFile, result);
		
		saveFile(request, file);
		
		JsonResponse jr = new JsonResponse();
		jr.setResponse("ok");
		return jr;
	}
	
	
	private void saveFile(HttpServletRequest request, MultipartFile file){
		InputStream inputStream = null;
		OutputStream outputStream = null;
		
		String fileName = file.getOriginalFilename();
		
		try {
			inputStream = file.getInputStream();

			String filePath = request.getRealPath("/");
			String relativePath = "/files/";

			File newFile = new File(filePath + relativePath + fileName);
			if (!newFile.exists()) {
				newFile.createNewFile();
			}

			outputStream = new FileOutputStream(newFile);
			int read = 0;
			byte[] bytes = new byte[1024];

			while ((read = inputStream.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}
			outputStream.close();
			ponerCurso(null, fileName);
		} catch (IOException e) {
			ponerCurso(null, "");
		}
		
		
	}

	
	private static Course sCourse = null;
	private static String sAttachedfile = null;
	private void ponerCurso(Course course, String attachedfile){
		
		if (course!=null){
			sCourse = course;
		} else if (attachedfile!=null) {
			sAttachedfile = attachedfile;
		}
		
		if (sCourse!=null && sAttachedfile!=null){
			
			if (!sAttachedfile.isEmpty()){
				sCourse.setAttachedfile(sAttachedfile);
			}
			
			mCc.saveCourse(sCourse);
			
			sCourse = null;
			sAttachedfile = null;
		}
		
	}
	
	

}