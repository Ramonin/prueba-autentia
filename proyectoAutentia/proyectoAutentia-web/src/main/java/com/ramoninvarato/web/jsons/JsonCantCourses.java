package com.ramoninvarato.web.jsons;

import java.io.Serializable;
import java.util.List;

import com.ramoninvarato.core.model.Course;

public class JsonCantCourses implements Serializable {

	private static final long serialVersionUID = -9203292212868859043L;
	
	private int maxPages;
	private List<Course> courses;
	
	public List<Course> getCourses() {
		return courses;
	}

	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}

	public int getMaxPages() {
		return maxPages;
	}

	public void setMaxPages(int maxPages) {
		this.maxPages = maxPages;
	}



}
