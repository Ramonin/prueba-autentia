package com.ramoninvarato.core.dao.mybatis;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.ramoninvarato.core.model.Course;
import com.ramoninvarato.core.model.Teacher;

/**
 * Clase de mapeo de las sentencias SQL para Mybatis
 * 
 * @author Ram�n Invarato
 * 
 */
public interface SqlMybatisMapper {

	public static final String TABLE_COURSES = "courses";
	public static final String TABLE_TEACHERS = "teachers";

	@Select("SELECT * FROM " + TABLE_COURSES +" WHERE enable=true LIMIT #{limit} OFFSET #{offset}")
	@Options(useCache = false)
	List<Course> selectSomeCourses(@Param("limit") int limit, @Param("offset") int offset);
	
	@Select("SELECT * FROM " + TABLE_COURSES + " WHERE enable=true ORDER BY #{atributeOrderBy} #{ascDesc} LIMIT #{limit} OFFSET #{offset}")
	@Options(useCache = false)
	List<Course> selectSomeCoursesByOrder(@Param("limit") int limit, @Param("offset") int offset, @Param("atributeOrderBy") String atributeOrderBy, @Param("ascDesc") String ascDesc);

	@Select("SELECT COUNT(*) FROM " + TABLE_COURSES +" WHERE enable=true ")
	@Options(useCache = false)
	int countAllCourses();
	
	@Insert("INSERT INTO "
			+ TABLE_COURSES
			+ " "
			+ "(title,level,hours,enable,attachedfile,idTeacherKeyForeign)"
			+ " VALUES (#{title},#{level},#{hours},#{enable},#{attachedfile},#{idTeacherKeyForeign})")
	@Options(useGeneratedKeys = true, keyProperty = "idCourse", flushCache = true)
	int insertCourse(Course course);

	@Select("SELECT * FROM " + TABLE_TEACHERS)
	@Options(useCache = true)
	List<Teacher> selectAllTechers();

}