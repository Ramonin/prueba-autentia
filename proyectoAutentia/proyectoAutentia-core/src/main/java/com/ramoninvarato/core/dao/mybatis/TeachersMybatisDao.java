package com.ramoninvarato.core.dao.mybatis;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ramoninvarato.core.dao.TeachersDao;
import com.ramoninvarato.core.model.Teacher;

@Repository
public class TeachersMybatisDao implements TeachersDao {

	@Autowired
	private SqlMybatisMapper mMapper;

	@Override
	public List<Teacher> searchAllTeachers() {
		return mMapper.selectAllTechers();
	}

}