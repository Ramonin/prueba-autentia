package com.ramoninvarato.core.dao;

import java.util.List;

import com.ramoninvarato.core.controller.CoursesController.ORDER;
import com.ramoninvarato.core.controller.CoursesController.TABLEBYORDER;
import com.ramoninvarato.core.model.Course;

public interface CoursesDao {

		List<Course> searchSomeCourses(int limit, int offset);
		
		List<Course> searchSomeCourses(int limit, int offset, TABLEBYORDER atributeOrderBy, ORDER ascDesc);
		
		int saveCourse(Course course);
		
		int countCourses();
}