package com.ramoninvarato.core.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.ramoninvarato.core.dao.TeachersDao;
import com.ramoninvarato.core.model.Teacher;

@Controller
public class TeachersController {

	@Autowired
	private TeachersDao mTeachersDao;

	public List<Teacher> searchAllTeachers() {
		return mTeachersDao.searchAllTeachers();
	}

}