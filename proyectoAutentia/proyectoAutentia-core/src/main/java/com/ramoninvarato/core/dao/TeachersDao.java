package com.ramoninvarato.core.dao;

import java.util.List;

import com.ramoninvarato.core.model.Teacher;

public interface TeachersDao {
		List<Teacher> searchAllTeachers();
}