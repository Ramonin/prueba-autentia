package com.ramoninvarato.core.model;

import java.io.Serializable;

public class Course implements Serializable {

	private static final long serialVersionUID = 8899557934514029191L;
	
	private long idCourse;
	private String title;
	private String level;
	private int hours;
	private boolean enable;
	private String attachedfile;
	private long idTeacherKeyForeign;
	
	public long getIdCourse() {
		return idCourse;
	}

	public void setIdCourse(long idCourse) {
		this.idCourse = idCourse;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public int getHours() {
		return hours;
	}

	public void setHours(int hours) {
		this.hours = hours;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public String getAttachedfile() {
		return attachedfile;
	}

	public void setAttachedfile(String attachedfile) {
		this.attachedfile = attachedfile;
	}

	public long getIdTeacherKeyForeign() {
		return idTeacherKeyForeign;
	}

	public void setIdTeacherKeyForeign(long idTeacherKeyForeign) {
		this.idTeacherKeyForeign = idTeacherKeyForeign;
	}



	public String toString() {
		return new StringBuilder(this.getClass().getName()+" #")
		.append(", idCourse:").append(idCourse)
		.append(", title:").append(title)
		.append(", level:").append(level)
		.append(", hours:").append(hours)
		.append(", enable:").append(enable)
		.append(", attachedfile:").append(attachedfile)
		.append(", idTeacherKeyForeign:").append(idTeacherKeyForeign)
		.toString();
	}

}
