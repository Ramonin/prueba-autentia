package com.ramoninvarato.core.model;

import java.io.Serializable;

public class Teacher implements Serializable {

	private static final long serialVersionUID = 4945010702366024372L;
	
	private long idTeacher;
	private String name;
	
	public long getIdTeacher() {
		return idTeacher;
	}

	public void setIdTeacher(long idTeacher) {
		this.idTeacher = idTeacher;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String toString() {
		return new StringBuilder(this.getClass().getName()+" #")
		.append(", idTeacher:").append(idTeacher)
		.append(", name:").append(name)
		.toString();
	}

}
