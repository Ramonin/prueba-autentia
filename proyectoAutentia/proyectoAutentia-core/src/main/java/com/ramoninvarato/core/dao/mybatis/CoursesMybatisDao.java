package com.ramoninvarato.core.dao.mybatis;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ramoninvarato.core.controller.CoursesController.ORDER;
import com.ramoninvarato.core.controller.CoursesController.TABLEBYORDER;
import com.ramoninvarato.core.dao.CoursesDao;
import com.ramoninvarato.core.model.Course;

@Repository
public class CoursesMybatisDao implements CoursesDao {

	@Autowired
	private SqlMybatisMapper mMapper;

	@Override
	public List<Course> searchSomeCourses(int limit, int offset) {
		return mMapper.selectSomeCourses(limit, offset);
	}

	@Override
	public int saveCourse(Course course) {
		return mMapper.insertCourse(course);
	}

	@Override
	public List<Course> searchSomeCourses(int limit, int offset, TABLEBYORDER atributeOrderBy, ORDER ascDesc) {
		
		System.out.println("atributeOrderBy: "+atributeOrderBy.toString()+" - ascDesc:"+ascDesc.toString());
		
		return mMapper.selectSomeCoursesByOrder(limit, offset, atributeOrderBy.toString(), ascDesc.toString());
	}

	@Override
	public int countCourses() {
		return mMapper.countAllCourses();
	}
	
}