package com.ramoninvarato.core.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.ramoninvarato.core.dao.CoursesDao;
import com.ramoninvarato.core.model.Course;

@Controller
public class CoursesController {
	
	public enum ORDER {
	    ASC ("ASC"),
	    DESC   ("DESC");

	    private final String value;
	    ORDER(String value) {
	        this.value = value;
	    }
	    
	    @Override
	    public String toString() {
	    	return this.value;
	    }
	}
	
	public enum TABLEBYORDER {
		title ("title"),
		hours ("hours"),
		level   ("level");

	    private final String value;
	    TABLEBYORDER(String value) {
	        this.value = value;
	    }
	    
	    @Override
	    public String toString() {
	    	return this.value;
	    }
	}
	

	@Autowired
	private CoursesDao mCoursesDao;

	public List<Course> searchCoursesByPage(int numPage) {
		int limit = 4;
		int offset = (numPage-1) * 4;
		return mCoursesDao.searchSomeCourses(limit, offset);
	}
	
	public List<Course> searchCoursesByPage(int numPage, TABLEBYORDER atributeOrderBy, ORDER ascDesc) {
		int limit = 4;
		int offset = (numPage-1) * 4;		
		
		return mCoursesDao.searchSomeCourses(limit, offset, atributeOrderBy, ascDesc);
	}
	
	public boolean saveCourse (Course course){
		return mCoursesDao.saveCourse(course)>0;
	}
	
	public int maxPages (){
		return (int) Math.ceil(((float) mCoursesDao.countCourses())/4);
	}


}