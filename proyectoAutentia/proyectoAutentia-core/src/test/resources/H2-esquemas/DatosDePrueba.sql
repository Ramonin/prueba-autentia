/* --- courses --- */
insert into courses
(idCourse, title,level,hours,enable,attachedfile,idTeacherKeyForeign)
values (1, 'PHP','Basico',30,1,'url/fileA',2);

insert into courses
(idCourse, title,level,hours,enable,attachedfile,idTeacherKeyForeign)
values (2, 'Java','Avanzado',20,0,'url/fileB',2);

insert into courses
(idCourse, title,level,hours,enable,attachedfile,idTeacherKeyForeign)
values (3, 'AngularJs','Intermedio',10,1,'url/fileC',3);

insert into courses
(idCourse, title,level,hours,enable,attachedfile,idTeacherKeyForeign)
values (4, 'Net','Basico',23,1,'url/fileD',1);

insert into courses
(idCourse, title,level,hours,enable,attachedfile,idTeacherKeyForeign)
values (5, 'Visual Basic','Intermedio',12,0,'url/fileE',3);

insert into courses
(idCourse, title,level,hours,enable,attachedfile,idTeacherKeyForeign)
values (6, 'Pascal','Avanzado',43,1,'url/fileF',3);
/* --- courses --- */

/* --- teachers --- */
insert into teachers
(idTeacher, name)
values (1,'Basico');

insert into teachers
(idTeacher, name)
values (2,'Intermedio');

insert into teachers
(idTeacher, name)
values (3,'Avanzado');
/* --- courses --- */