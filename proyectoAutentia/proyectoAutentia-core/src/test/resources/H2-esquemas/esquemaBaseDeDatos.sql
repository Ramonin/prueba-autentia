DROP TABLE courses IF EXISTS;
CREATE TABLE courses(
	idCourse INT PRIMARY KEY, 
	title VARCHAR(50), 
	level VARCHAR(50), 
	hours INT,
	enable TINYINT(1),
	attachedfile VARCHAR(200), 
	idTeacherKeyForeign INT
);

DROP TABLE teachers IF EXISTS;
CREATE TABLE teachers(
	idTeacher INT PRIMARY KEY, 
	name VARCHAR(50) NOT NULL
);