-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 18-07-2014 a las 23:11:44
-- Versión del servidor: 5.6.16
-- Versión de PHP: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `autentia_table`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `courses`
--

CREATE TABLE IF NOT EXISTS `courses` (
  `idCourse` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL DEFAULT '',
  `level` enum('Básico','Intermedio','Avanzado') NOT NULL DEFAULT 'Básico',
  `hours` int(11) NOT NULL DEFAULT '0',
  `enable` tinyint(1) NOT NULL DEFAULT '1',
  `attachedfile` varchar(50) DEFAULT NULL,
  `idTeacherKeyForeign` bigint(20) NOT NULL,
  PRIMARY KEY (`idCourse`),
  KEY `FOREIGN` (`idTeacherKeyForeign`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Volcado de datos para la tabla `courses`
--

INSERT INTO `courses` (`idCourse`, `title`, `level`, `hours`, `enable`, `attachedfile`, `idTeacherKeyForeign`) VALUES
(3, 'Introducción a JSF2', 'Intermedio', 25, 1, NULL, 1),
(5, 'Novedades en Spring 3', 'Intermedio', 16, 1, NULL, 2),
(7, 'Java', 'Avanzado', 45, 0, NULL, 3),
(9, 'PHP', 'Avanzado', 78, 1, NULL, 4),
(10, 'Android', 'Intermedio', 46, 1, NULL, 5),
(12, 'iOs', 'Intermedio', 8, 1, NULL, 2),
(13, 'HTML', 'Básico', 19, 1, NULL, 3),
(14, 'CSS', 'Básico', 26, 1, NULL, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `teachers`
--

CREATE TABLE IF NOT EXISTS `teachers` (
  `idTeacher` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`idTeacher`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `teachers`
--

INSERT INTO `teachers` (`idTeacher`, `name`) VALUES
(1, 'Juan'),
(2, 'Paco'),
(3, 'Raúl'),
(4, 'Ramón'),
(5, 'Iñigo');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `courses`
--
ALTER TABLE `courses`
  ADD CONSTRAINT `courses_ibfk_1` FOREIGN KEY (`idTeacherKeyForeign`) REFERENCES `teachers` (`IdTeacher`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
